EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "zx-mini-pcb-debug-module-ay-3-8912-v1"
Date ""
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C4
U 1 1 5FFF84FE
P 9250 4350
F 0 "C4" H 9300 4450 50  0000 L CNN
F 1 "0.1uF" H 9300 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9288 4200 50  0001 C CNN
F 3 "~" H 9250 4350 50  0001 C CNN
	1    9250 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 607AC187
P 3550 2350
F 0 "R1" V 3450 2400 50  0000 R CNN
F 1 "470" V 3650 2400 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 2350 50  0001 C CNN
F 3 "~" H 3550 2350 50  0001 C CNN
	1    3550 2350
	1    0    0    1   
$EndComp
$Comp
L Device:LED D1
U 1 1 607AC18D
P 3550 2650
F 0 "D1" H 3550 2550 50  0000 C CNN
F 1 "TO-1608BC-MRE" H 3543 2486 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 3550 2650 50  0001 C CNN
F 3 "https://www.oasistek.com/pdf/TO-1608BC-MRE_9201T098.pdf" H 3550 2650 50  0001 C CNN
	1    3550 2650
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 607DAFBC
P 9600 4550
F 0 "#PWR012" H 9600 4300 50  0001 C CNN
F 1 "GND" H 9605 4377 50  0000 C CNN
F 2 "" H 9600 4550 50  0001 C CNN
F 3 "" H 9600 4550 50  0001 C CNN
	1    9600 4550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J4
U 1 1 6086846F
P 2750 4600
F 0 "J4" H 2750 5300 50  0000 C CNN
F 1 "PLS-14" H 2750 3800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 2750 4600 50  0001 C CNN
F 3 "~" H 2750 4600 50  0001 C CNN
	1    2750 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J2
U 1 1 60868447
P 1750 4600
F 0 "J2" H 1750 5300 50  0000 C CNN
F 1 "PBS-14" H 1750 3800 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 1750 4600 50  0001 C CNN
F 3 "~" H 1750 4600 50  0001 C CNN
	1    1750 4600
	-1   0    0    -1  
$EndComp
$Comp
L lib:AY-3-8912 D2
U 1 1 605DF88F
P 8050 2850
F 0 "D2" H 8550 3115 50  0000 C CNN
F 1 "AY-3-8912" H 8550 3024 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 8200 3050 50  0001 C CNN
F 3 "" H 8200 3050 50  0001 C CNN
	1    8050 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C7
U 1 1 605E39A7
P 9950 4350
F 0 "C7" H 10068 4396 50  0000 L CNN
F 1 "10uF" H 10068 4305 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-18_Kemet-A" H 9988 4200 50  0001 C CNN
F 3 "~" H 9950 4350 50  0001 C CNN
	1    9950 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 605E5770
P 9600 4350
F 0 "C6" H 9650 4450 50  0000 L CNN
F 1 "1uF" H 9650 4250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9638 4200 50  0001 C CNN
F 3 "~" H 9600 4350 50  0001 C CNN
	1    9600 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4550 9250 4550
Wire Wire Line
	9250 4550 9250 4500
Wire Wire Line
	9250 4550 9600 4550
Wire Wire Line
	9600 4550 9600 4500
Connection ~ 9250 4550
Wire Wire Line
	9600 4550 9950 4550
Wire Wire Line
	9950 4550 9950 4500
Connection ~ 9600 4550
Wire Wire Line
	9050 4150 9250 4150
Wire Wire Line
	9250 4150 9250 4200
Wire Wire Line
	9250 4150 9600 4150
Wire Wire Line
	9600 4150 9600 4200
Connection ~ 9250 4150
Wire Wire Line
	9600 4150 9950 4150
Wire Wire Line
	9950 4150 9950 4200
Connection ~ 9600 4150
$Comp
L power:+5V #PWR022
U 1 1 605E7ED1
P 9600 4150
F 0 "#PWR022" H 9600 4000 50  0001 C CNN
F 1 "+5V" H 9615 4323 50  0000 C CNN
F 2 "" H 9600 4150 50  0001 C CNN
F 3 "" H 9600 4150 50  0001 C CNN
	1    9600 4150
	1    0    0    -1  
$EndComp
NoConn ~ 9050 3950
NoConn ~ 9050 3850
NoConn ~ 9050 3750
NoConn ~ 9050 3650
NoConn ~ 9050 3550
NoConn ~ 9050 3450
NoConn ~ 9050 3350
NoConn ~ 9050 3250
NoConn ~ 8050 4550
Wire Wire Line
	9050 2850 9650 2850
Wire Wire Line
	9050 2950 9650 2950
Wire Wire Line
	9050 3050 9650 3050
Text Label 9400 2850 0    50   ~ 0
AC_A
Text Label 9400 2950 0    50   ~ 0
AC_B
Text Label 9400 3050 0    50   ~ 0
AC_C
Wire Wire Line
	7600 3550 8050 3550
Text Label 7600 3550 0    50   ~ 0
CLOCK
$Comp
L Device:R R2
U 1 1 605ED34B
P 7250 2500
F 0 "R2" V 7150 2550 50  0000 R CNN
F 1 "4.7k" V 7350 2550 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7180 2500 50  0001 C CNN
F 3 "~" H 7250 2500 50  0001 C CNN
	1    7250 2500
	1    0    0    1   
$EndComp
$Comp
L power:+5V #PWR021
U 1 1 605EE18E
P 7250 2350
F 0 "#PWR021" H 7250 2200 50  0001 C CNN
F 1 "+5V" H 7265 2523 50  0000 C CNN
F 2 "" H 7250 2350 50  0001 C CNN
F 3 "" H 7250 2350 50  0001 C CNN
	1    7250 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 605EEAAF
P 7550 2500
F 0 "R3" V 7450 2550 50  0000 R CNN
F 1 "4.7k" V 7650 2550 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7480 2500 50  0001 C CNN
F 3 "~" H 7550 2500 50  0001 C CNN
	1    7550 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7250 2350 7550 2350
Connection ~ 7250 2350
Wire Wire Line
	7250 3350 7250 2650
Wire Wire Line
	7550 3150 7550 2650
$Comp
L Device:R R4
U 1 1 605F43AB
P 7850 2500
F 0 "R4" V 7750 2550 50  0000 R CNN
F 1 "4.7k" V 7950 2550 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7780 2500 50  0001 C CNN
F 3 "~" H 7850 2500 50  0001 C CNN
	1    7850 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7550 2350 7850 2350
Connection ~ 7550 2350
Wire Wire Line
	7850 2650 7850 2950
Wire Wire Line
	7850 2950 8050 2950
Wire Wire Line
	7550 3150 8050 3150
Wire Wire Line
	7250 3350 8050 3350
Text Label 6950 2850 0    50   ~ 0
BC1
Text Label 6950 3050 0    50   ~ 0
BDIR
Text Label 6950 3750 0    50   ~ 0
DA0
Text Label 6950 3850 0    50   ~ 0
DA1
Text Label 6950 3950 0    50   ~ 0
DA2
Text Label 6950 4050 0    50   ~ 0
DA3
Text Label 6950 4150 0    50   ~ 0
DA4
Text Label 6950 4250 0    50   ~ 0
DA5
Text Label 6950 4350 0    50   ~ 0
DA6
Text Label 6950 4450 0    50   ~ 0
DA7
$Comp
L power:GND #PWR015
U 1 1 60607C0B
P 5000 3950
F 0 "#PWR015" H 5000 3700 50  0001 C CNN
F 1 "GND" H 5005 3777 50  0000 C CNN
F 2 "" H 5000 3950 50  0001 C CNN
F 3 "" H 5000 3950 50  0001 C CNN
	1    5000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 4450 6750 4450
Wire Wire Line
	8050 4350 6750 4350
Wire Wire Line
	8050 4250 6750 4250
Wire Wire Line
	8050 4150 6750 4150
Wire Wire Line
	8050 4050 6750 4050
Wire Wire Line
	8050 3950 6750 3950
Wire Wire Line
	8050 3850 6750 3850
Wire Wire Line
	8050 3750 6750 3750
$Comp
L power:GND #PWR014
U 1 1 6060AA84
P 4650 2500
F 0 "#PWR014" H 4650 2250 50  0001 C CNN
F 1 "GND" H 4655 2327 50  0000 C CNN
F 2 "" H 4650 2500 50  0001 C CNN
F 3 "" H 4650 2500 50  0001 C CNN
	1    4650 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60609742
P 4650 2350
F 0 "C2" H 4700 2450 50  0000 L CNN
F 1 "0.1uF" H 4700 2250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4688 2200 50  0001 C CNN
F 3 "~" H 4650 2350 50  0001 C CNN
	1    4650 2350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC595 U1
U 1 1 6060223F
P 5000 3250
F 0 "U1" H 5100 3900 50  0000 C CNN
F 1 "74HC595" H 5200 3800 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 5000 3250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 5000 3250 50  0001 C CNN
	1    5000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2650 5000 2200
Wire Wire Line
	5000 2200 4650 2200
$Comp
L Device:C C1
U 1 1 6060E7D6
P 4350 2350
F 0 "C1" H 4400 2450 50  0000 L CNN
F 1 "1uF" H 4400 2250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4388 2200 50  0001 C CNN
F 3 "~" H 4350 2350 50  0001 C CNN
	1    4350 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2200 4650 2200
Connection ~ 4650 2200
Wire Wire Line
	4350 2500 4650 2500
Connection ~ 4650 2500
Wire Wire Line
	4600 2850 4300 2850
$Comp
L power:+5V #PWR013
U 1 1 606150A5
P 4650 2200
F 0 "#PWR013" H 4650 2050 50  0001 C CNN
F 1 "+5V" H 4665 2373 50  0000 C CNN
F 2 "" H 4650 2200 50  0001 C CNN
F 3 "" H 4650 2200 50  0001 C CNN
	1    4650 2200
	1    0    0    -1  
$EndComp
Text Label 4300 2850 0    50   ~ 0
DATA
$Comp
L power:GND #PWR011
U 1 1 60617DB8
P 4600 3450
F 0 "#PWR011" H 4600 3200 50  0001 C CNN
F 1 "GND" H 4605 3277 50  0000 C CNN
F 2 "" H 4600 3450 50  0001 C CNN
F 3 "" H 4600 3450 50  0001 C CNN
	1    4600 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 3350 4300 3350
Text Label 4300 3350 0    50   ~ 0
STROB
Wire Wire Line
	4600 3050 4300 3050
Text Label 4300 3050 0    50   ~ 0
CLK
$Comp
L power:+5V #PWR010
U 1 1 6061DF38
P 4600 3150
F 0 "#PWR010" H 4600 3000 50  0001 C CNN
F 1 "+5V" H 4615 3323 50  0000 C CNN
F 2 "" H 4600 3150 50  0001 C CNN
F 3 "" H 4600 3150 50  0001 C CNN
	1    4600 3150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 6064B12A
P 6350 4850
F 0 "#PWR020" H 6350 4600 50  0001 C CNN
F 1 "GND" H 6355 4677 50  0000 C CNN
F 2 "" H 6350 4850 50  0001 C CNN
F 3 "" H 6350 4850 50  0001 C CNN
	1    6350 4850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 6064B130
P 5700 3500
F 0 "#PWR016" H 5700 3250 50  0001 C CNN
F 1 "GND" H 5705 3327 50  0000 C CNN
F 2 "" H 5700 3500 50  0001 C CNN
F 3 "" H 5700 3500 50  0001 C CNN
	1    5700 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 6064B136
P 6000 3350
F 0 "C5" H 6050 3450 50  0000 L CNN
F 1 "0.1uF" H 6050 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6038 3200 50  0001 C CNN
F 3 "~" H 6000 3350 50  0001 C CNN
	1    6000 3350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC595 U2
U 1 1 6064B13C
P 6350 4150
F 0 "U2" H 6450 4800 50  0000 C CNN
F 1 "74HC595" H 6550 4700 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 6350 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74hc595.pdf" H 6350 4150 50  0001 C CNN
	1    6350 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3550 6350 3200
Wire Wire Line
	6350 3200 6000 3200
$Comp
L Device:C C3
U 1 1 6064B144
P 5700 3350
F 0 "C3" H 5750 3450 50  0000 L CNN
F 1 "1uF" H 5750 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5738 3200 50  0001 C CNN
F 3 "~" H 5700 3350 50  0001 C CNN
	1    5700 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3200 6000 3200
Connection ~ 6000 3200
Wire Wire Line
	5700 3500 6000 3500
$Comp
L power:+5V #PWR019
U 1 1 6064B14F
P 6000 3200
F 0 "#PWR019" H 6000 3050 50  0001 C CNN
F 1 "+5V" H 6015 3373 50  0000 C CNN
F 2 "" H 6000 3200 50  0001 C CNN
F 3 "" H 6000 3200 50  0001 C CNN
	1    6000 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 6064B156
P 5950 4350
F 0 "#PWR018" H 5950 4100 50  0001 C CNN
F 1 "GND" H 5955 4177 50  0000 C CNN
F 2 "" H 5950 4350 50  0001 C CNN
F 3 "" H 5950 4350 50  0001 C CNN
	1    5950 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4250 5650 4250
Text Label 5650 4250 0    50   ~ 0
STROB
Wire Wire Line
	5950 3950 5650 3950
Text Label 5650 3950 0    50   ~ 0
CLK
$Comp
L power:+5V #PWR017
U 1 1 6064B160
P 5950 4050
F 0 "#PWR017" H 5950 3900 50  0001 C CNN
F 1 "+5V" H 5965 4223 50  0000 C CNN
F 2 "" H 5950 4050 50  0001 C CNN
F 3 "" H 5950 4050 50  0001 C CNN
	1    5950 4050
	0    -1   -1   0   
$EndComp
NoConn ~ 5400 3050
NoConn ~ 5400 3150
NoConn ~ 5400 3250
NoConn ~ 5400 3350
NoConn ~ 5400 3450
NoConn ~ 5400 3550
Wire Wire Line
	5400 3750 5950 3750
Connection ~ 5700 3500
Wire Wire Line
	5400 2950 6350 2950
Wire Wire Line
	6350 2950 6350 3050
Wire Wire Line
	6350 3050 8050 3050
Wire Wire Line
	5400 2850 8050 2850
Wire Wire Line
	6750 4650 7000 4650
Wire Wire Line
	7000 4650 7000 5150
Wire Wire Line
	7000 5150 4300 5150
Text Label 4300 5150 0    50   ~ 0
QH`
Wire Wire Line
	4350 2200 3550 2200
Connection ~ 4350 2200
$Comp
L power:GND #PWR09
U 1 1 606D9C25
P 3550 2800
F 0 "#PWR09" H 3550 2550 50  0001 C CNN
F 1 "GND" H 3555 2627 50  0000 C CNN
F 2 "" H 3550 2800 50  0001 C CNN
F 3 "" H 3550 2800 50  0001 C CNN
	1    3550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 4200 2550 4200
Wire Wire Line
	1950 4300 2550 4300
Wire Wire Line
	1950 4400 2550 4400
Wire Wire Line
	1950 4500 2550 4500
Wire Wire Line
	1950 4600 2550 4600
Wire Wire Line
	1950 4700 2550 4700
Wire Wire Line
	1950 4800 2550 4800
Wire Wire Line
	1950 4900 2550 4900
Text Label 2150 4200 0    50   ~ 0
DA0
Text Label 2150 4300 0    50   ~ 0
DA1
Text Label 2150 4400 0    50   ~ 0
DA2
Text Label 2150 4500 0    50   ~ 0
DA3
Text Label 2150 4600 0    50   ~ 0
DA4
Text Label 2150 4700 0    50   ~ 0
DA5
Text Label 2150 4800 0    50   ~ 0
DA6
Text Label 2150 4900 0    50   ~ 0
DA7
$Comp
L power:GND #PWR04
U 1 1 60700AEC
P 1950 5300
F 0 "#PWR04" H 1950 5050 50  0001 C CNN
F 1 "GND" V 1950 5100 50  0000 C CNN
F 2 "" H 1950 5300 50  0001 C CNN
F 3 "" H 1950 5300 50  0001 C CNN
	1    1950 5300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 60702C7D
P 2550 5300
F 0 "#PWR08" H 2550 5050 50  0001 C CNN
F 1 "GND" V 2550 5100 50  0000 C CNN
F 2 "" H 2550 5300 50  0001 C CNN
F 3 "" H 2550 5300 50  0001 C CNN
	1    2550 5300
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 60705467
P 1950 4000
F 0 "#PWR03" H 1950 3850 50  0001 C CNN
F 1 "+5V" V 1900 4150 50  0000 C CNN
F 2 "" H 1950 4000 50  0001 C CNN
F 3 "" H 1950 4000 50  0001 C CNN
	1    1950 4000
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR07
U 1 1 607075DD
P 2550 4000
F 0 "#PWR07" H 2550 3850 50  0001 C CNN
F 1 "+5V" V 2600 4150 50  0000 C CNN
F 2 "" H 2550 4000 50  0001 C CNN
F 3 "" H 2550 4000 50  0001 C CNN
	1    2550 4000
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J3
U 1 1 60713872
P 2750 2900
F 0 "J3" H 2750 3600 50  0000 C CNN
F 1 "PLS-14" H 2750 2100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 2750 2900 50  0001 C CNN
F 3 "~" H 2750 2900 50  0001 C CNN
	1    2750 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J1
U 1 1 60713878
P 1750 2900
F 0 "J1" H 1750 3600 50  0000 C CNN
F 1 "PBS-14" H 1750 2100 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 1750 2900 50  0001 C CNN
F 3 "~" H 1750 2900 50  0001 C CNN
	1    1750 2900
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6071388E
P 1950 3600
F 0 "#PWR02" H 1950 3350 50  0001 C CNN
F 1 "GND" V 1950 3400 50  0000 C CNN
F 2 "" H 1950 3600 50  0001 C CNN
F 3 "" H 1950 3600 50  0001 C CNN
	1    1950 3600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60713894
P 2550 3600
F 0 "#PWR06" H 2550 3350 50  0001 C CNN
F 1 "GND" V 2550 3400 50  0000 C CNN
F 2 "" H 2550 3600 50  0001 C CNN
F 3 "" H 2550 3600 50  0001 C CNN
	1    2550 3600
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 6071389A
P 1950 2300
F 0 "#PWR01" H 1950 2150 50  0001 C CNN
F 1 "+5V" V 1950 2500 50  0000 C CNN
F 2 "" H 1950 2300 50  0001 C CNN
F 3 "" H 1950 2300 50  0001 C CNN
	1    1950 2300
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 607138A0
P 2550 2300
F 0 "#PWR05" H 2550 2150 50  0001 C CNN
F 1 "+5V" V 2550 2500 50  0000 C CNN
F 2 "" H 2550 2300 50  0001 C CNN
F 3 "" H 2550 2300 50  0001 C CNN
	1    2550 2300
	0    -1   -1   0   
$EndComp
Text Label 2150 3200 0    50   ~ 0
QH`
Text Label 2150 3000 0    50   ~ 0
STROB
Text Label 2150 3100 0    50   ~ 0
CLK
Text Label 2150 2900 0    50   ~ 0
DATA
Wire Wire Line
	1950 3100 2550 3100
Wire Wire Line
	1950 3400 2550 3400
Wire Wire Line
	1950 3200 2550 3200
Wire Wire Line
	1950 3000 2550 3000
Wire Wire Line
	1950 2900 2550 2900
Text Label 2150 5000 0    50   ~ 0
BC1
Text Label 2150 5100 0    50   ~ 0
BDIR
Wire Wire Line
	1950 2500 2550 2500
Wire Wire Line
	1950 2600 2550 2600
Wire Wire Line
	1950 2700 2550 2700
Text Label 2150 2500 0    50   ~ 0
AC_A
Text Label 2150 2600 0    50   ~ 0
AC_B
Text Label 2150 2700 0    50   ~ 0
AC_C
Wire Wire Line
	1950 5100 2550 5100
Wire Wire Line
	1950 5000 2550 5000
Text Label 2150 3400 0    50   ~ 0
CLOCK
Wire Wire Line
	1950 4100 1950 4000
Connection ~ 1950 4000
Wire Wire Line
	2550 4100 2550 4000
Connection ~ 2550 4000
Wire Wire Line
	1950 5200 1950 5300
Connection ~ 1950 5300
Wire Wire Line
	2550 5200 2550 5300
Connection ~ 2550 5300
Wire Wire Line
	1950 2400 1950 2300
Connection ~ 1950 2300
Wire Wire Line
	2550 2400 2550 2300
Connection ~ 2550 2300
$Comp
L power:GND #PWR0101
U 1 1 60612467
P 2550 2800
F 0 "#PWR0101" H 2550 2550 50  0001 C CNN
F 1 "GND" V 2550 2600 50  0000 C CNN
F 2 "" H 2550 2800 50  0001 C CNN
F 3 "" H 2550 2800 50  0001 C CNN
	1    2550 2800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60615101
P 1950 2800
F 0 "#PWR0102" H 1950 2550 50  0001 C CNN
F 1 "GND" V 1950 2600 50  0000 C CNN
F 2 "" H 1950 2800 50  0001 C CNN
F 3 "" H 1950 2800 50  0001 C CNN
	1    1950 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 3500 1950 3600
Connection ~ 1950 3600
Wire Wire Line
	2550 3500 2550 3600
Connection ~ 2550 3600
$Comp
L power:GND #PWR0103
U 1 1 60627997
P 1950 3300
F 0 "#PWR0103" H 1950 3050 50  0001 C CNN
F 1 "GND" V 1950 3100 50  0000 C CNN
F 2 "" H 1950 3300 50  0001 C CNN
F 3 "" H 1950 3300 50  0001 C CNN
	1    1950 3300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 6062A859
P 2550 3300
F 0 "#PWR0104" H 2550 3050 50  0001 C CNN
F 1 "GND" V 2550 3100 50  0000 C CNN
F 2 "" H 2550 3300 50  0001 C CNN
F 3 "" H 2550 3300 50  0001 C CNN
	1    2550 3300
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J5
U 1 1 60630E9F
P 3150 2900
F 0 "J5" H 3150 3600 50  0000 C CNN
F 1 "PLS-14" H 3150 2100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 3150 2900 50  0001 C CNN
F 3 "~" H 3150 2900 50  0001 C CNN
	1    3150 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2300 2950 2300
Wire Wire Line
	2550 2400 2950 2400
Connection ~ 2550 2400
Wire Wire Line
	2950 2500 2550 2500
Connection ~ 2550 2500
Wire Wire Line
	2550 2600 2950 2600
Connection ~ 2550 2600
Wire Wire Line
	2950 2700 2550 2700
Connection ~ 2550 2700
Wire Wire Line
	2550 2800 2950 2800
Connection ~ 2550 2800
Wire Wire Line
	2950 2900 2550 2900
Connection ~ 2550 2900
Wire Wire Line
	2550 3000 2950 3000
Connection ~ 2550 3000
Wire Wire Line
	2950 3100 2550 3100
Connection ~ 2550 3100
Wire Wire Line
	2550 3200 2950 3200
Connection ~ 2550 3200
Wire Wire Line
	2950 3300 2550 3300
Connection ~ 2550 3300
Wire Wire Line
	2550 3400 2950 3400
Connection ~ 2550 3400
Wire Wire Line
	2950 3500 2550 3500
Connection ~ 2550 3500
Wire Wire Line
	2550 3600 2950 3600
$Comp
L Connector_Generic:Conn_01x14 J6
U 1 1 6066654C
P 3150 4600
F 0 "J6" H 3150 5300 50  0000 C CNN
F 1 "PLS-14" H 3150 3800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x14_P2.54mm_Vertical" H 3150 4600 50  0001 C CNN
F 3 "~" H 3150 4600 50  0001 C CNN
	1    3150 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4000 2950 4000
Wire Wire Line
	2950 4100 2550 4100
Connection ~ 2550 4100
Wire Wire Line
	2550 4200 2950 4200
Wire Wire Line
	2950 4300 2550 4300
Wire Wire Line
	2550 4400 2950 4400
Wire Wire Line
	2950 4500 2550 4500
Wire Wire Line
	2550 4600 2950 4600
Wire Wire Line
	2950 4700 2550 4700
Wire Wire Line
	2550 4800 2950 4800
Wire Wire Line
	2950 4900 2550 4900
Wire Wire Line
	2550 5000 2950 5000
Wire Wire Line
	2950 5100 2550 5100
Wire Wire Line
	2550 5200 2950 5200
Connection ~ 2550 5200
Wire Wire Line
	2950 5300 2550 5300
Connection ~ 2550 5000
Connection ~ 2550 5100
Connection ~ 2550 4200
Connection ~ 2550 4300
Connection ~ 2550 4400
Connection ~ 2550 4500
Connection ~ 2550 4600
Connection ~ 2550 4700
Connection ~ 2550 4800
Connection ~ 2550 4900
$EndSCHEMATC
